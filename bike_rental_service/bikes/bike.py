'''
bike_rental_service/
    setup.py
    README
        __init__.py
        bike_rental_service/
                  __int__.py
                  bikes/
                        __init__.py
                        bike.py
                  accessories/
                        __init__.py
                        bike_map.py
                        battery.py
'''
class BikeGearException(Exception):
    pass

class BikeBellException(Exception):
    pass


class Bike(object):
    '''Bike is the base class for bikes.'''

    def __init__(self, nb_of_gears=5):
        self.nb_of_gears = nb_of_gears
        self.wheel_diameter = 28.0
        self.is_electric = False
        self.bell_sound = "PING!"
        self.saddle_height = 13.0
        
    def bell(self):
        print(self.bell_sound)

    def set_nb_of_gears(self, nb_of_gears):
        if nb_of_gears >= 0:
            self.nb_of_gears = nb_of_gears
        else:
            raise BikeGearException("Wrong nb of gears")

    #def set_saddle_height(....)
        
if __name__ == '__main__':
    bike_1 = Bike()
    try:
        bike_1.set_nb_of_gears(6)
        print("Saddle height is now {:f}".format(bike_1.saddle_height))
    except BikeGearException as val_expt:
        print(expt)
    except TypeError as type_expt:
        print("Invalid type")
    except Exception as expt:
        print(expt)
        
